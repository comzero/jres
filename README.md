# JRes
-----
## Introduction

> This is `resource` collections for leran __Javascript__.


## Resource
- [MDN A re-introduction to JavaScript zh-CN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
- [MDN JavaScript-en](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [MDN Javascript-zh-CN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript)
- [Quirksmode Javascript](http://www.quirksmode.org/js/contents.html)
- [NodeSchool](http://nodeschool.io)
- [Resource on Github](https://github.com/sethvincent/javascripting)
- ### To be Continued